class University {
  final String name;
  final String country;
  final String webPage;

  University(
    this.name,
    this.country,
    this.webPage,
  );

  University.dto(Map map)
      : name = map['name'],
        country = map['country'],
        webPage = map['web_pages'].first;
}
