import 'package:api_practice_2/data/repositories/services/network_service.dart';
import 'package:api_practice_2/models/university.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  String name;
  int lenght = 0;
  List<University> filtred = [];
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    var querySize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('Universiry of Ukraine'),
      ),
      body: FutureBuilder(
          future: createUniversity(),
          builder: (context, AsyncSnapshot<List<University>> snapshot) {
            if (snapshot.hasData) {
              return SingleChildScrollView(
                physics: NeverScrollableScrollPhysics(),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: SizedBox(
                          height: querySize.height * 0.05,
                          width: double.infinity,
                          child: TextFormField(
                            onEditingComplete: () {
                              FocusScope.of(context).unfocus();
                              setState(() {
                                print(widget.name);
                              });

                              setState(() {
                                snapshot.data.map((e) {
                                  if (e.name.contains(widget.name) &&
                                      widget.name != null &&
                                      widget.name != '') {
                                    widget.filtred.add(e);
                                  }
                                }).toList();
                              });
                            },
                            onChanged: (value) {
                              setState(
                                () {
                                  if (value != null) {
                                    return widget.name = value;
                                  }
                                  return widget.filtred = [];
                                },
                              );
                            },
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Find university',
                            ),
                          )),
                    ),
                    SizedBox(
                      height: querySize.height * 0.8,
                      child: (widget.name == null) || widget.name == ''
                          ? ListView.builder(
                              itemCount: snapshot.data.length,
                              itemBuilder: (context, index) => Card(
                                elevation: 5,
                                color:
                                    index % 2 == 0 ? Colors.blue : Colors.white,
                                child: ListTile(
                                  leading: Icon(Icons.school),
                                  title: Text(snapshot.data[index].name),
                                  subtitle: Text(
                                    snapshot.data[index].webPage,
                                    style: TextStyle(
                                        color: index % 2 == 0
                                            ? Colors.white.withOpacity(0.8)
                                            : Colors.blue.withOpacity(0.8)),
                                  ),
                                  trailing: Text(
                                      'Country ${snapshot.data[index].country}'),
                                ),
                              ),
                            )
                          : ListView.builder(
                              itemCount: widget.filtred.length,
                              itemBuilder: (context, index) => Card(
                                elevation: 5,
                                color:
                                    index % 2 == 0 ? Colors.blue : Colors.white,
                                child: ListTile(
                                  leading: Icon(Icons.school),
                                  title: Text(widget.filtred[index].name),
                                  subtitle: Text(
                                    widget.filtred[index].webPage,
                                    style: TextStyle(
                                        color: index % 2 == 0
                                            ? Colors.white.withOpacity(0.8)
                                            : Colors.blue.withOpacity(0.8)),
                                  ),
                                  trailing: Text(
                                      'Country ${widget.filtred[index].country}'),
                                ),
                              ),
                            ),
                    ),
                  ],
                ),
              );
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          }),
    );
  }
}
