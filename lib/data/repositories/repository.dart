import 'package:http/http.dart' as http;

Future<String> getData() {
  return http
      .get('http://universities.hipolabs.com/search?country=Ukraine')
      .then((value) => value.body);
}
