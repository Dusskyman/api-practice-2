import 'dart:convert';

import 'package:api_practice_2/data/repositories/repository.dart';
import 'package:api_practice_2/models/university.dart';

Future<List<University>> createUniversity() async {
  var listData = jsonDecode(await getData()) as List;
  return listData.map((e) => University.dto(e)).toList();
}
